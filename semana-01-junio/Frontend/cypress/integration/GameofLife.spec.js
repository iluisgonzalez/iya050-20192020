describe('Home', () => {
    it('Visit Home without crashing', () => {
        cy.visit('http://localhost:3000')
    })
})

describe('Home elements interactions', () => {
    it('Interact with Home elements without crashing', () => {
        cy.visit('http://localhost:3000')
        cy.get('[data-testid="Board"]')
            .should('exist')
            .click()
        cy.get('[data-testid="controls"]')
            .should('exist')
            .click()
        cy.get('[data-testid="start-button"]')
            .click()
        cy.get('[data-testid="stop-button"]')
            .click()
        cy.get('[data-testid="TimeInput"]')
            .type('1000')
        
    })
})