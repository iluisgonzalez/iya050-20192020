import React from 'react';
import './GameofLife.css';


const CELL_SIZE = 20;
const WIDTH = 800;
const HEIGHT = 600;


class Cell extends React.Component {

    render() {
        const { x, y } = this.props;
        return (
            <div className="Cell" style={{
                left: `${CELL_SIZE * x + 1}px`,
                top: `${CELL_SIZE * y + 1}px`,
                width: `${CELL_SIZE - 1}px`,
                height: `${CELL_SIZE - 1}px`,
            }} />
        );
    }
}


class GameofLife extends React.Component {

    constructor() {
        super();
        this.rows = HEIGHT / CELL_SIZE;
        this.cols = WIDTH / CELL_SIZE;

        this.board = this.makeEmptyBoard();
    }

    state = {
        cells: [],
        isRunning: false,
        interval: 1000,

    }
    //make empty board array 2D
    makeEmptyBoard() {
        let board = [];
        for (let y = 0; y < this.rows; y++) {
            board[y] = [];
            for (let x = 0; x < this.cols; x++) {
                board[y][x] = false;
            }
        }

        return board;
    }

    //create object of cell position
    getElementOffset() {
        const rect = this.boardRef.getBoundingClientRect();
        const doc = document.documentElement;

        return {
            x: (rect.left + window.pageXOffset) - doc.clientLeft,
            y: (rect.top + window.pageYOffset) - doc.clientTop,
        };
    }

    //create cells and save in array cells
    makeCells() {
        let cells = [];
        for (let y = 0; y < this.rows; y++) {
            for (let x = 0; x < this.cols; x++) {
                if (this.board[y][x]) {
                    cells.push({ x, y });
                }
            }
        }

        return cells;
    }

    //true or false cells in board
    handleClick = (event) => {

        const elemOffset = this.getElementOffset();
        const offsetX = event.clientX - elemOffset.x;
        const offsetY = event.clientY - elemOffset.y;

        const x = Math.floor(offsetX / CELL_SIZE);
        const y = Math.floor(offsetY / CELL_SIZE);

        if (x >= 0 && x <= this.cols && y >= 0 && y <= this.rows) {
            this.board[y][x] = !this.board[y][x];
        }

        this.setState({ cells: this.makeCells() });
    }

    runGame = () => {
        this.setState({ isRunning: true });
        this.runIteration();
    }

    stopGame = () => {
        this.setState({ isRunning: false });
        if (this.timeoutHandler) {
            window.clearTimeout(this.timeoutHandler);
            this.timeoutHandler = null;
        }
    }
    //make calls here
    async runIteration() {
        let newBoard = this.makeEmptyBoard();

        for (let y = 0; y < this.rows; y++) {
            for (let x = 0; x < this.cols; x++) {
                if (this.board[y][x]) {
                    newBoard[y][x] = 1;
                } else {
                    newBoard[y][x] = 0;
                }
            }
        }
        await this.sendData(newBoard)
        await this.updateData(newBoard)

        this.setState({ cells: this.makeCells() });

        this.timeoutHandler = window.setTimeout(() => {
            this.runIteration();
        }, this.state.interval);
    }

    //seconds to next generation
    handleIntervalChange = (event) => {
        this.setState({ interval: event.target.value });
    }
    //clear cells
    handleClear = () => {
        this.board = this.makeEmptyBoard();
        this.setState({ cells: this.makeCells() });
    }

    sendData = (newBoardData) => {
        fetch("http://localhost:8080/life", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(newBoardData)
        }).then(response => response.json())
        .then(data => {
            console.log(data);
        })
    }

    async updateData(newBoardData) {
        //let evolution = [[0, 0, 0],[1, 1, 1],[0, 0, 0]];
        let evolution = newBoardData;
        //let evolution = await this.getData();
        fetch("http://localhost:8080/life/next", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(evolution)
        }).then(response => response.json())
        .then(data => {
            this.board = data;
            for (let y = 0; y < this.rows; y++) {
                for (let x = 0; x < this.cols; x++) {
                    if (this.board[y][x] === 1) {
                        this.board[y][x] = true;
                    } else {
                        this.board[y][x] = false;
                    }
                }
            }
            console.log(this.board)
        })
    }

    async getData() {
       await fetch("http://localhost:8080/life", {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            },
        }).then(response => response.json())
        .then(data => {
            return data;
        })
    }

    render() {
        const { cells, isRunning } = this.state; //interval as var
        return (
            <div>
                <div data-testid="Board" className="Board"
                    style={{ width: WIDTH, height: HEIGHT, backgroundSize: `${CELL_SIZE}px ${CELL_SIZE}px` }}
                    onClick={this.handleClick}
                    ref={(n) => { this.boardRef = n; }}>

                    {cells.map(cell => (
                        <Cell x={cell.x} y={cell.y} key={`${cell.x},${cell.y}`} />
                    ))}
                </div>

                <div data-testid="controls" className="controls">
                    Update every <input data-testid="TimeInput" value={this.state.interval} onChange={this.handleIntervalChange} /> msec
                    {isRunning ?
                        <button data-testid="stop-button" className="button" onClick={this.stopGame}>Stop</button> :
                        <button data-testid="start-button" className="button" onClick={this.runGame}>Run</button>
                    }
                    <button data-testid="clear-button" className="button" onClick={this.handleClear}>Clear</button>
                </div>
            </div>
        );
    }
}


export default GameofLife;