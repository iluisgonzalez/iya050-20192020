
import React, { Component } from 'react';
import './App.css';
import GameofLife from './GameofLife';

class App extends Component {

    render() {

        return (
            <div className="App">
                <h1>Conway's Game of Life</h1>
                <GameofLife />
            </div>
        );
    }
}

export default App;