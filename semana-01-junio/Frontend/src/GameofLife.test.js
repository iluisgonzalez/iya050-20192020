import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import GameofLife from './GameofLife';
import ReactDOM from 'react-dom';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<GameofLife />, div);
});