import React from 'react';
import ReactDOM from 'react-dom';
import { render, fireEvent } from '@testing-library/react';
import App from './App';

it('renders without without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
});

it('renders divs elements without crashing', () => {
    const { queryByTestId } = render(<App />)
    
    expect(queryByTestId("Board")).toBeTruthy()
    expect(queryByTestId("controls")).toBeTruthy()
});

it('renders time input evolution without crashing', () => {
    const { queryByTestId } = render(<App />)
    
    const time_input = queryByTestId("TimeInput");

    fireEvent.change(time_input, {target: {value: "1000"}})
    expect(time_input.value).toBe("1000")
});
